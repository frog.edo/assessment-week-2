const theAverage = (input) => {
    let total = 0
    let average

    for(let i = 0; i < input.length; i++){
        total += input[i]
    }
    average = total / input.length
    return average
}


const input = [1, 2, 3, 4, 5];

console.log(theAverage(input));
