const countWord = (search, str) => {
  return str.toLowerCase().split(search.toLowerCase()).length - 1
};

const wordToCount = "dog";
const input = "Dogdogcatdogsheep";


console.log(countWord(wordToCount, input));
